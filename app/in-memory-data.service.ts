export class InMemoryDataService {
  createDb() {
    let heroes = [
      { id: 11, name: 'Mr. Nice' , Mating : 'Narco', Age : '11/5/2012' , Species :'American Bullforg', Gender: 'Male', Health:'Yes' },
      { id: 12, name: 'Narco',Mating : 'Mr. Nice', Age : '21/5/2012' , Species :'American Bullforg', Gender: 'Female' , Health:'Yes'},
      { id: 13, name: 'Bombasto',Mating : 'Celeritas', Age : '21/5/2012' , Species :'American Bullforg', Gender: 'Male' , Health:'Yes' },
      { id: 14, name: 'Celeritas',Mating : 'Bombasto', Age : '21/5/2012' , Species :'American Bullforg' , Gender: 'Female', Health:'Yes' },
      { id: 15, name: 'Magneta',Mating : 'Neta', Age : '21/5/2012' , Species :'American Bullforg', Gender: 'Male' , Health:'Yes'},
      { id: 16, name: 'RubberMan',Mating : 'TheFatty', Age : '21/5/2012', Species :'American Bullforg', Gender: 'Male' , Health:'Yes' },
      { id: 17, name: 'Dynama' , Mating : 'Magneta',Age : '21/5/2012', Species :'Common Frog', Gender: 'Female', Health:'Yes' },
      { id: 18, name: 'Dr IQ',Mating : 'TheFatty', Age : '21/5/2012' , Species :'Common Frog', Gender: 'Male', Health:'Yes' },
      { id: 19, name: 'Magma',Mating : 'Narco', Age : '21/5/2012', Species :'Common Frog' , Gender: 'Female', Health:'Yes' },
      { id: 20, name: 'Tornado',Mating : 'Asto',Age : '21/5/2012' , Species :'Common Frog', Gender: 'Male', Health:'Yes' },
      { id: 21, name: 'Mr.Ahme', Mating : 'Tika',Age : '21/5/2012' , Species :'Common Frog', Gender: 'Female' , Health:'No'},
      { id: 22, name: 'Nice', Mating : 'Lokas',Age : '21/5/2012' , Species :'Common Frog', Gender: 'Male', Health:'Yes' },
      { id: 23, name: 'Asto', Mating : 'Narco',Age : '21/5/2012' , Species :'Daruma pond', Gender: 'Female', Health:'Yes' },
      { id: 24, name: 'Ritas', Mating : 'Lipa',Age : '21/5/2012' , Species :'Daruma pond', Gender: 'Male', Health:'Yes' },
      { id: 25, name: 'Neta' , Mating : 'Mr.Ahme',Age : '02/7/2016', Species :'Daruma pond', Gender: 'Female' , Health:'Yes'},
      { id: 26, name: 'Reman', Mating : 'Tika',Age : '02/7/2016' , Species :'Daruma pond', Gender: 'Female', Health:'No' },
      { id: 27, name: 'Liz', Mating : 'Nila',Age : '02/7/2016' , Species :'Daruma pond', Gender: 'Male', Health:'Yes' },
      { id: 28, name: 'Chen' , Mating : 'Narco',Age : '02/7/2016' , Species :'Daruma pond', Gender: 'Male', Health:'Yes'},
      { id: 29, name: 'Toro', Mating : 'Narco',Age : '02/7/2016' , Species :'Wood', Gender: 'Female', Health:'Yes' },
      { id: 30, name: 'Tika', Mating : 'Liki',Age : '02/7/2016' , Species :'Wood' , Gender: 'Male', Health:'Yes'},
      { id: 31, name: 'Lokas', Mating : 'Magneta',Age : '02/7/2016', Species :'Wood', Gender: 'Female', Health:'Yes' },
      { id: 32, name: 'Nila' , Mating : 'Narco',Age : '02/7/2016' , Species :'Wood', Gender: 'Female', Health:'Yes'},
      { id: 33, name: 'Rajus', Mating : 'Neta',Age : '02/7/2016' , Species :'Spring Peeper', Gender: 'Male', Health:'Yes' },
      { id: 34, name: 'Kilos', Mating : 'Lipa',Age : '02/7/2016' , Species :'Spring Peeper', Gender: 'Male', Health:'Yes' },
      { id: 35, name: 'Copaa', Mating : 'Narco',Age : '02/7/2016' , Species :'Spring Peeper', Gender: 'Male' , Health:'No'},
      { id: 36, name: 'Lipa', Mating : 'Momos',Age : '02/7/2016'  , Species :'Spring Peeper', Gender: 'Female', Health:'Yes'},
      { id: 37, name: 'Jaso' , Mating : 'Narco',Age : '02/7/2016' , Species :'California red legged', Gender: 'Male', Health:'Yes'},
      { id: 38, name: 'TheFatty', Mating : 'Narco',Age : '02/7/2016' , Species :'California red legged', Gender: 'Female', Health:'Yes'},
      { id: 39, name: 'Momos',Mating : 'Liki', Age : '02/7/2016' , Species :'California red legged', Gender: 'Male', Health:'Yes'},
      { id: 40, name: 'Liki',Mating : 'Momos', Age : '02/7/2016', Species :'California red legged', Gender: 'Female' , Health:'Yes' }
    ];
    return { heroes };
  }
}
