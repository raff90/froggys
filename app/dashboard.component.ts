import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccordionConfig } from 'ngx-bootstrap/accordion';

import { Hero } from './hero';
import { HeroService } from './hero.service';

@Component({
  moduleId: module.id,
  selector: 'my-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['style.css']
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
  public customClass: string = 'customClass';
  public isFirstOpen: boolean = true;
  public Species = [];

  constructor(
    private router: Router,
    private heroService: HeroService) {
  }

  ngOnInit(): void {
    this.heroService.getHeroes()
      .then(heroes => {
        this.heroes = heroes;
        console.log(heroes);
        for(var i of heroes)
        {
             this.Species.push(i.Species);
        }
        console.log(this.Species);

      
    });
    
  }

  gotoDetail(hero: Hero): void {
    let link = ['/detail', hero.id];
    this.router.navigate(link);
  }
  toFrogList()
  {
    this.router.navigate(['/heroes']);
  }
  refreshDashboard()
  {
    this.router.navigate(['/dashboard']);
  }
}
