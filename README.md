# Getting Started

## Get the Code
```
git https://raff90@bitbucket.org/raff90/froggys.git
npm i
```

## Add Library

to Add ng2-bootstrap,DataTable, run  

```
npm i ngx-bootstrap --save
npm i @swimlane/ngx-datatable --save
```




### Just in Time (JiT) Compilation

Runs the TypeScript compiler and launches the app

```
npm start
```

### Ahead of Time (AoT) Compilation 

Runs the Angular AoT compiler, rollup, uglify for an optimized bundle, then launches the app

```
npm run start-aot
```

### AoT + gzip 

Runs AoT plus gzips and launches the app 

```
gulp copy-aot-gzip
npm run aot
npm run rollup
http-server
```

Notes:
- Use your favorite server in place of `http-server`
- This could be scripted, obviously
- `lite-server` does not launch gzipped files by default.


